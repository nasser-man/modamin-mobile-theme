<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Modamin_mobile_theme
 */

?>

	</div><!-- #content -->


	<div id="offcanvas-flip" uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">

            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <h3>Title</h3>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			<hr/>

			<?php
			wp_nav_menu( array(
				'theme_location' => 'modamin-mobile-menu'
			) );
			?>

        </div>
    </div>




	<?php if(is_front_page() or true) { ?>
	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'modamin-mobile-theme' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'modamin-mobile-theme' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'modamin-mobile-theme' ), 'modamin-mobile-theme', '<a href="http://ressan.ir">Nasser.man@gmail.com</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<?php } ?>
</div><!-- #page -->




<?php wp_footer(); ?>

<?php if(is_front_page() and false) { ?>
	<h1>IS FRONT PAGE</h1>
<script>
jQuery(document).ready(function($) {
    var mainContent = $("#content");
	var siteUrl = "http://" + top.location.host.toString();
	var url = '';
	var url_of_page = "";



    // $(document).delegate("a[href^='"+siteUrl+"'], a[href^='/'], a[href^='./'], a[href^='../'], a[href^='#']", "click", function(e) {
    $(document).on("click", "a", function() {
		UIkit.offcanvas("#offcanvas-flip").hide();
		var goto_link = $(this).attr("href");
		// location.hash = goto_link
		console.log(goto_link);
		// return false;
		$("div#primary.place-for-ajax-loaded-content").html('<div id="loading">Loading the next page for you.  Please stand by!</div>').load(goto_link, function() {
			UIkit.offcanvas("#offcanvas-flip").hide();
		});

		return false;

    });

    $("#searchform").submit(function(e) {
        location.hash = '?s=' + $("#s").val();
        e.preventDefault();
    });

    $(window).bind('hashchange', function(){
		UIkit.offcanvas("#offcanvas-flip").hide();
        url = window.location.hash.substring(1);
		url_of_page = url;

        if (!url) {
            return;
        }

        url = url + " #content";

        // mainContent.html('<div id="loading">Loading the next page for you.  Please stand by!</div>').load(url, function() {});
        $("div#primary.place-for-ajax-loaded-content").html('<div id="loading">Loading the next page for you.  Please stand by!</div>').load(url, function() {
			UIkit.offcanvas("#offcanvas-flip").hide();
		});




		// var head_of_page = "";
		//
		// var q = $.ajax({
		// 	url : url_of_page,
		// 	type : "GET"
		// });
		//
		// q.fail(function(){
		// 	console.log('failed');;
		// });
		//
		// q.done(function(response){
		// 	// console.log($("#content" , response));
		// 	// head_of_page = jQuery(response).find('head').html();
		// 	// $('head').html(head_of_page);
		// 	// mainContent.html($("#content" , response));
		// 	// $('head').html(jQuery(response).find('head').html());
		// 	// mainContent.html(jQuery(response).find('#primary').html());
		// 	$("div#primary.place-for-ajax-loaded-content").html(response);
		// });

    });

    $(window).trigger('hashchange');
});
</script>
<?php } ?>

</body>
</html>
