<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Modamin_mobile_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- UIkit CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.6/css/uikit.min.css" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'modamin-mobile-theme' ); ?></a>


	<nav id="modamin-top-navbar" class="uk-navbar-container" uk-navbar>
		<div class="uk-navbar-right">
			<a href=""  uk-icon="icon: cart; ratio: 2" uk-toggle="target: #offcanvas-flip"></a>
			<a href=""  uk-icon="icon: search; ratio: 2" uk-toggle="target: #offcanvas-flip"></a>
			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="#">Active</a></li>
				<li>
					<a href="#">Parent</a>
					<div class="uk-navbar-dropdown">
						<ul class="uk-nav uk-navbar-dropdown-nav">
							<li class="uk-active"><a href="#">Active</a></li>
							<li><a href="#">Item</a></li>
							<li><a href="#">Item</a></li>
						</ul>
					</div>
				</li>
				<li><a href="#">Item</a></li>
			</ul>
		</div>


		<div class="uk-navbar-center">
			<a class="uk-navbar-item uk-logo" href="#">
				<img src="<?php echo get_template_directory_uri().'/images/modamin-logo.png' ?>" />
			</a>
		</div>

		<div class="uk-navbar-left">
			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="#">Active</a></li>
				<li>
					<a href="#">Parent</a>
					<div class="uk-navbar-dropdown">
						<ul class="uk-nav uk-navbar-dropdown-nav">
							<li class="uk-active"><a href="#">Active</a></li>
							<li><a href="#">Item</a></li>
							<li><a href="#">Item</a></li>
						</ul>
					</div>
				</li>
				<li><a href="#">Item</a></li>
			</ul>
			<a href="#"  uk-icon="icon: menu; ratio: 1.5" uk-toggle="target: #offcanvas-flip"></a>
		</div>

	</nav>






	<div id="content" class="site-content">
